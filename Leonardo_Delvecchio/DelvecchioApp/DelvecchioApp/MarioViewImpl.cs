﻿using System.Drawing;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
    public class MarioViewImpl : IMarioView
    {
        private Rectangle rectangle;

        public MarioViewImpl()
        {
            rectangle = new Rectangle();
        }

        public void SetHeight(double height)
        {
            rectangle.Height = (int) height;
        }

        public void SetImg(string img)
        {
            Image imageFile = Image.FromFile(img);
            TextureBrush textureBrush = new TextureBrush(imageFile);
            Graphics graphics = Graphics.FromImage(imageFile);
            graphics.FillRectangle(textureBrush, rectangle);
        }

        public void SetPosition(double x, double y)
        {
            rectangle.X = (int) x;
            rectangle.Y = (int) y;
        }

        public void SetWidth(double width)
        {
            rectangle.Width = (int) width;
        }

        public Rectangle GetR
        {
            get { return rectangle; }
        }

        public void UpdatePosition(double y)
        {
            rectangle.Y = (int) y;
        }
    }
}
