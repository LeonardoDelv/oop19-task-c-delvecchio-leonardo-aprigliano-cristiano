﻿using System;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
	interface IMarioController
	{
		void StartMarioView();
		void ChangeMarioSkin(MarioType type);
		void MarioMovement(double n);
	}
}
