﻿using System;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
	interface IMarioModel
	{
		MarioType Type { get; }
		string MarioRunImgPath { get; }
		string MarioJumpImgPath { get; }
		string MarioPowerImgPath { get; }
		string MarioPowerJumpImgPath { get; }
		double Width { get; }
		double Height { get; }
		double PosY { get; }
		double PosX { get; }
		void MarioUpdate(double n);
	}
}
