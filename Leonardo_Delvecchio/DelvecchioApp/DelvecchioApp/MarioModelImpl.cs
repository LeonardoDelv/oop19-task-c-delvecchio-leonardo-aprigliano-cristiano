﻿using System;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
    public class MarioModelImpl : IMarioModel
    {

        private static readonly double INITIAl_POS_Y = 200.0;
        private static readonly double INITIAL_POS_X = 20.0;
        private static readonly double MARIO_WIDTH = 63.0;
        private static readonly double MARIO_HEIGHT = 85.0;

        public double Width { get; private set; }
        public double Height { get; private set; }
        public string MarioRunImgPath { get; }
        public string MarioJumpImgPath { get; }
        public string MarioPowerImgPath { get; }
        public string MarioPowerJumpImgPath { get; }
        public double PosY { get; private set; }
        public double PosX { get; private set; }
        public MarioType Type { get; set; }

        public MarioModelImpl()
        {
            Type = MarioType.RUN;
            PosX = INITIAL_POS_X;
            PosY = INITIAl_POS_Y;
            Width = MARIO_WIDTH;
            MarioRunImgPath = "images/mario.png";
            MarioJumpImgPath = "images/mario_jump.png";
            MarioPowerImgPath = "images/luigi_power_up.png";
            MarioPowerJumpImgPath = "images/luigi_jump.png";
        }

        public void MarioUpdate(double n)
        {
            PosY = PosY + n;
        }
    }
}