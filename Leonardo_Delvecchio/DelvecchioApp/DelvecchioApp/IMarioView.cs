﻿using System;
using System.Drawing;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
	interface IMarioView
	{
		void SetWidth(double width);
		void SetHeight(double height);
		void SetImg(string img);
		void SetPosition(double x, double y);
		void UpdatePosition(double y);
	}
}
