﻿using System;

namespace Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp
{
	public class MarioControllerImpl : IMarioController
	{
        private MarioModelImpl marioModelImpl;
        private MarioViewImpl marioViewImpl;

        public MarioControllerImpl()
        {
            marioModelImpl = new MarioModelImpl();
            marioViewImpl = new MarioViewImpl();
            StartMarioView();
        }

        public MarioViewImpl MarioView
        {
            get { return marioViewImpl; }
        }

        public MarioModelImpl GetMarioModel
        {
            get { return marioModelImpl; }
        }

        public void ChangeMarioSkin(MarioType type)
        {
            if (type == MarioType.RUN)
            {
                marioViewImpl.SetImg(marioModelImpl.MarioRunImgPath);
            }
            else if (type == MarioType.JUMP)
            {
                marioViewImpl.SetImg(marioModelImpl.MarioJumpImgPath);
            }
            else if (type == MarioType.POWER)
            {
                marioViewImpl.SetImg(marioModelImpl.MarioPowerImgPath);
            }
            else
            {
                marioViewImpl.SetImg(marioModelImpl.MarioPowerJumpImgPath);
            }
        }

        public void MarioMovement(double n)
        {
            marioViewImpl.UpdatePosition(marioModelImpl.PosY);
            marioModelImpl.MarioUpdate(n);
        }

        public void StartMarioView()
        {
            marioViewImpl.SetHeight(marioModelImpl.Height);
            marioViewImpl.SetWidth(marioModelImpl.Width);
            marioViewImpl.SetImg(marioModelImpl.MarioRunImgPath);
            marioViewImpl.SetPosition(marioModelImpl.PosX, marioModelImpl.PosY);
        }
    }
}