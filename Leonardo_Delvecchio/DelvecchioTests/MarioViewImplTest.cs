﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp;
using System.Drawing;

namespace Leonardo_Delvecchio.DelvecchioTests
{
    [TestClass]
    public class MarioViewImplTest
    {
        [TestMethod]
        public void SetWidthTest()
        {
            double width = 10.0;

            MarioViewImpl marioViewImpl = new MarioViewImpl();
            Rectangle rectangle = new Rectangle();

            marioViewImpl.SetWidth(width);
            rectangle.Width = width;

            Assert.IsTrue(rectangle.Width == marioViewImpl.GetR.Width, "Width not set correctly");
        }
        
        [TestMethod]
        public void SetHeightTest()
        {
            double height = 30.0;

            MarioViewImpl marioViewImpl = new MarioViewImpl();
            Rectangle rectangle = new Rectangle();

            marioViewImpl.SetHeight(height);
            rectangle.Height = height;

            Assert.IsTrue(rectangle.Height == marioViewImpl.GetR.Height, "Height not set correctly");
        }
    }
}
