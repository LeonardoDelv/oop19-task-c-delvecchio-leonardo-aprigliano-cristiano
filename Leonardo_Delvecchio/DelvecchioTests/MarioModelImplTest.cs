using Microsoft.VisualStudio.TestTools.UnitTesting;
using Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp;

namespace Leonardo_Delvecchio.DelvecchioTests
{
    [TestClass]
    public class MarioModelImplTest
    {
        [TestMethod]
        public void MarioUpdateTest()
        {
            double n = 10.0;
            double x = 60.0;
            double expected;

            MarioModelImpl marioModel = new MarioModelImpl();
            marioModel.PosY = x;
            expected = marioModel.PosY + n;
            marioModel.PosY = expected;
            Assert.IsTrue(expected == marioModel.PosY, "MarioUpdate() doesn't work");
        }
    }
}
