﻿using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Leonardo_Delvecchio.DelvecchioApp.DelvecchioApp;

namespace Leonardo_Delvecchio.DelvecchioTests
{
    [TestClass]
    public class MarioControllerImplTest
    {
        [TestMethod]
        public void MarioMovementTest()
        {
            double n = 5.0;

            MarioModelImpl marioModelImpl = new MarioModelImpl();
            MarioViewImpl marioViewImpl = new MarioViewImpl();
            Rectangle rectangle = new Rectangle();

            marioModelImpl.MarioUpdate(n);
            marioViewImpl.UpdatePosition(marioModelImpl.PosY);
            Assert.IsTrue(marioViewImpl.GetR.Y == marioModelImpl.PosY, "MarioMovement() doesn't work");
        }

        [TestMethod]
        public void ChangeMarioSkinTest()
        {
            MarioControllerImpl marioControllerImpl = new MarioControllerImpl();

            marioControllerImpl.MarioChangeSkin(MarioType.POWER);
            Assert.IsTrue(marioControllerImpl.GetMarioModel.Type == MarioType.POWER, "MarioChangeSkin() doesn't work");
        }
    }
}
