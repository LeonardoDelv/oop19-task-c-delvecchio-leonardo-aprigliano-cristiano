using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nicholas_ciarafoni.CiarafoniApp;
using System.Windows.Shapes.Shape;

namespace FloorControllerImplTest
{
    [TestClass]
    public class FloorControllerImplTest
    {
        [TestMethod]
        public void StartFloorView()
        {
            FloorControllerImpl floorController = new FloorControllerImpl();
            FloorModelImpl floorModel = new FloorModelImpl();
            FloorViewImplTest floorView = new FloorViewImplTest();

            floorView.SetImg(floorModel.FloorImgPath());
            floorView.SetHeight(floorModel.FloorHeight());
            floorView.SetWidth(floorModel.FloorWidth());
            floorView.SetPosition(floorModel.FloorPosX(), floorModel.FloorPosY());

            Assert.IsNotNull(FloorControllerImplTest.StartFloorView(), "Error in the function StartFloorView()");

        }
    } 
}