using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Nicholas_ciarafoni.CiarafoniApp;
using System.Windows.Shapes.Shape;

namespace FloorViewImplTest
{
    [TestClass]
    public class FloorViewImplTest
    {
        private const double V = 35.0;
        private const double T = 60.0;

        [TestMood]
        public void SetWidth ()
        {   
            //Testing Width
            double width = V;
            FloorViewImpl floorViewImpl = new FloorViewImpl();
            Rectangle rect = new Rectangle();
            floorViewImpl.SetWidth(width);
            rect.width= width;
            Assert.IsTrue (rect.width == floorViewImpl.GetR.Hidth, "Error on setting Width of Floor Object");

            //Testing Height
            double height = T;
            FloorViewImpl floorViewImpl = new FloorViewImpl();
            Rectangle rect = new Rectangle();
            floorViewImpl.SetHeight(height);
            rect.height= height;
            Assert.IsTrue (rect.height == floorViewImpl.GetR.Height, "Error on setting Height of Floor Object");
        }
    }
}

