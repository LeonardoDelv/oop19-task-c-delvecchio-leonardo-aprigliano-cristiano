using System.Drawing;
using System.Windows.Shapes.Shape;

namespace Nicholas_ciarafoni.CiarafoniApp
{
    public class FloorModelImpl : IFloorModel
    {
        private static readonly double POS_X = 0.0;
        private static readonly double POS_Y = 340.0;
        private static readonly double WIDTH = 600;
        private static readonly double HEIGHT = 60.0;
        private static readonly string floorImgPath;
        private readonly double posX;
        private readonly double posY;
        public FloorModelImpl() 
        {
            this.posX = POS_X;
            this.posY = POS_Y;
            this.floorImgPath = "images/floor.jpg";
        }
        public string FloorImgPath => this.floorImgPath;
        public double FloorPosX => this.posX;
        public double FloorPosY => this.posY;
        public double FloorWidth => this.WIDTH;
        public double FloorHeight => this.HEIGHT;
    }
}