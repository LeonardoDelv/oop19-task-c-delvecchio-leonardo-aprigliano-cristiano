using System.Drawing;
using System.Windows.Shapes.Shape;

namespace Nicholas_Ciarafoni.CiarafoniApp
{
    public class FloorControllerImpl : IFloorController
    {
            private readonly IFloorView floorView;
            private readonly IFloorModel floorModel;

            public FloorControllerImpl() 
            {
                this.floorView = new FloorViewImpl();
                this.floorModel = new FloorModelImpl();
                this.StartFloorView();
            }

            public void StartFloorView() 
            {
                floorView.SetImg(floorModel.FloorImgPath());
                floorView.SetHeight(floorModel.FloorHeight());
                floorView.SetWidth(floorModel.FloorWidth());
                floorView.SetPosition(floorModel.FloorPosX(), floorModel.FloorPosY());
            }
        public IFloorView FloorView => this.floorView;
        public IFloorModel FloorModel => this.floorModel;
    }
}