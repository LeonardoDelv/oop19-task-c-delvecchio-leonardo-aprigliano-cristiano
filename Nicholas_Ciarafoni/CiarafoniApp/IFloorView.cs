using System.Drawing;

namespace Nicholas_Ciarafoni.CiarafoniApp
{
    interface IFloorView
    {
        void SetWidth(double width);
        void SetHeight(double height);
        void SetImg(string img);
        Rectangle Floor { get; }
        void SetPosition(double x, double y);
    }
}