
namespace Nicholas_Ciarafoni.CiarafoniApp
{
    interface IFloorController
    {
        void StartFloorView();
        IFloorView FloorView { get; }
        IFloorModel FloorModel { get; }
    }
}