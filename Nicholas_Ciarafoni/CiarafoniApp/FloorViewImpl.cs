using System.Drawing;
using System.Windows.Shapes.Shape;

namespace Nicholas_Ciarafoni.CiarafoniApp
{
    public class FloorViewImpl : IFloorView
    {
        private readonly Rectangle rect;
        public FloorViewImpl() => this.rect = new Rectangle();

        public void SetWidth(double width) => rect.SetWidth(width);
        public void SetHeight(double height) => rect.SetHeight(height);
        public void SetImg(string img) => rect.SetFill(new ImagePattern(new Image(img)));

        public Rectangle Floor => this.rect;
        public void SetPosition(double x, double y) 
        {
            rect.SetY(y);
            rect.SetX(x);
        }



    }
}