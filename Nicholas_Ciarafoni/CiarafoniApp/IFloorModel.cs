
namespace Nicholas_ciarafoni.CiarafoniApp
{
    interface IFloorModel
    {
        string FloorImgPath { get; }

        double FloorPosX { get; }

        double FloorPosY { get; }

        double FloorWidth { get; }

        double FloorHeight { get; }
    }
}