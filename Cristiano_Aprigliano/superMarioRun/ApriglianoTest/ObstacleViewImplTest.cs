using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cristiano_Aprigliano.superMarioRun.ApriglianoApp;

namespace Cristiano_Aprigliano.ApriglianoTest
{
    [TestClass]
    public class ObstacleViewImplTest
    {
        [TestMethod]
        public void SetObstacleDimensionTest()
        {
            double width = 30.0;
            double height = 35.0;

            ObstacleViewImpl obstacleViewImpl = new ObstacleViewImpl();
            Rectangle rectangle = new Rectangle();

            obstacleViewImpl.SetObstacleDimension(30.0, 35.0);
            rectangle.Width = width;
            rectangle.Height = height; 

            Assert.isTrue(rectangle.Width == obstacleViewImpl.GetObstacle().Width, "Width hasn't been set correctly");
            Assert.isTrue(rectangle.Height == obstacleViewImpl.GetObstacle().Height, "Height hasn't been set correctly");
        }
    }
}
