using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cristiano_Aprigliano.superMarioRun.ApriglianoApp;

namespace Cristiano_Aprigliano.ApriglianoTest
{
    [TestClass]
    public class ObstacleControllerImplTest
    {
        [TestMethod]
        public void GenerateObstacleTest()
        {
            ObstacleControllerImpl obstacleTest = new ObstacleControllerImpl();

            obstacleTest.GenerateRandomObstacle();
            Assert.IsNotNull(obstacleTest.GetObstacleList(), "GenerateRandomObstacle() didn't worked properly");
        }
    }
}
