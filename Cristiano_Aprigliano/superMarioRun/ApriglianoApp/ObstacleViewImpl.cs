using System.Drawing;

namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    public class ObstacleViewImpl : IObstacleView
    {
        private Rectangle rectangle;

        public ObstacleViewImpl()
        {
            rectangle = new Rectangle();
        }

        public void SetObstaclePosition(double x, double y)
        {
            rectangle.X = x; 
            rectangle.Y = y;
        }

        public void SetObstacleDimension(double width, double height)
        {
            rectangle.Width = width;
            rectangle.Height = height;
        }

        public void SetImg(string img) 
        {
            Image imageFile = Image.FromFile(img);
            TextureBrush textureBrush = new TextureBrush(imageFile);
            Graphics graphics = Graphics.FromImage(imageFile);
            graphics.FillRectangle(textureBrush, rectangle);
        }

        public Rectangle GetObstacle()
        {
            return this.rectangle;
        }

        public void UpdatePos(double x)
        {
            rectangle.X(rectangle.GetPosX() - x);
        }

    }
}