using System.Drawing;

namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    public class ObstacleImpl : IObstacle
    {
        private double height;
        private double width;
        private double posX;
        private double posY;
        private string imagePath;
        private bool powerUp;

        public ObstacleImpl(double height, double width, double posX, double posY, string imagePath)
        {
            this.height = height;
            this.width = width;
            this.posX = posX; 
            this.posY = posY;
            this.imagePath = imagePath;
        }

        public double GetHeight()
        {
            return this.height;
        }

        public double GetWidth()
        {
            return this.width;
        }

        public double GetPosX()
        {
            return this.posX;
        }

        public double GetPosY()
        {
            return this.posY;
        }

        public double SetPosX()
        {
            this.posX = posX;
        }

        public double SetPosY()
        {
            this.posY = posY;
        }

        public double GetHeight()
        {
            return this.height;
        }

        public double GetWidth()
        {
            return this.width;
        }

        public string GetImagePath()
        {
            return this.imagePath;
        }

        public bool GetPowerUp()
        {
            return this.powerUp;
        }

        public void SetPowerUp(bool power)
        {
            this.powerUp = powerUp;
        }
    }
}