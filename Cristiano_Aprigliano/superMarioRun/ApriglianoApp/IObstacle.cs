using System.Drawing;

namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    /*
    This Interface is for the purpose of the Obstacles
    */
    interface IObstacle 
    {
        //This method returns the X Coordinate of the Obstacle
        double GetPosX();

        //This method returns the Y Coordinate of the Obstacle
        double GetPosY();

        //This method Sets the X Coordinate of the Obstacle
        double SetPosX(double PosX);

        //This method Sets the Y Coordinate of the Obstacle
        double SetPosY(double PosY);

        //This Method returns the Height of the Obstacle
        double GetHeight();

        //This Method returns the Width of the Obstacle
        double GetWidth();

        //This Method gets the Path of the Image
        String GetImagePath();

        //This Method gets true or false based on the PowerUp
        bool GetPowerUp();

        //This Method Sets the PowerUp
        void SetPowerUp(bool power);
    }
}