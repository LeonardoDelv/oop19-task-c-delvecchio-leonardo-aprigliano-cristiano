namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    interface IObstacleView
    {
        //This Method Sets the X, Y Coordinates of the Obstacle
        void SetObstaclePosition(double x, double y);

        //This Method Sets the Width, Height of the Obstacle
        void SetObstacleDimension(double width, double height);

        //This Method Sets the Image of the Obstacle
        void SetImg(String img);

        //This Method returns the Obstacle
        Rectangle GetObstacle(); 

        //This Method Updates the Position of the Obstacle on the X AXIS
        void UpdatePos(double x);
    }
}