using System.Drawing;

namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    public class ObstacleControllerImpl : IObstacleController
    {
        private static int obstaclesQuantity = 100;
        private static double moltiplication = 2.5; 
        private static double randomCactus = 0.30;
        private static double randomFlower = 0.60;
        private static double randomMushroom = 0.90;
        private List<ObstacleImpl> obstacleList;


        public ObstacleControllerImpl()
        {
            this.obstacleList = new ArrayList<>();
        }
        public List<ObstacleImpl> GenerateRandomObstacle()
        {
            for (int i = 0; i < obstaclesQuantity; i++)
            {
                Obstacle obstacle;
                double random = Math.random();
                if (random < randomCactus)
                {
                    obstacle = this.GenerateCactus();
                } else if (random >= randomCactus && random < randomFlower)
                {
                    obstacle = this.GenerateFlower();
                } else if (random >= randomFlower && random < randomMushroom)
                {
                    obstacle = this.GenerateMushroom();
                } else 
                {
                    obstacle = this.GeneratePowerUpMushroom();
                }
                this.obstacleList.add(obstacle);
            }
        }

        public List<ObstacleImpl> GetObstacleList()
        {
            return this.obstacleList;
        }

        public Obstacle GenerateMushroom()
        {
            ObstacleImpl mushroom = null;
            try
            {
                LoadJSONobstacleMushroom();
            } catch (IOException e)
            {
                Console.WriteLine("Exception has been thrown", e.GetType().Name);
            }
        }

        public Obstacle GenerateFlower()
        {
            ObstacleImpl flower = null;
            try
            {
                LoadJSONobstacleFlower();
            } catch (IOException e)
            {
                Console.WriteLine("Exception has been thrown", e.GetType().Name);
            }
        }

        public Obstacle GenerateCactus()
        {
            ObstacleImpl cactus = null;
            try
            {
                LoadJSONobstacleCactus();
            } catch (IOException e)
            {
                Console.WriteLine("Exception has been thrown", e.GetType().Name);
            }
        }

        public Obstacle GeneratePowerUpMushroom()
        {
            ObstacleImpl mushroomPower = null;
            try
            {
                LoadJSONobstaclePowerUpMushroom();
            } catch (IOException e)
            {
                Console.WriteLine("Exception has been thrown", e.GetType().Name);
            }
        }


        //This Method handles the Load and read of the Mushroom Obstacle JSON File
        private void LoadJSONobstacleMushroom()
        {
            using (StreamReader r = new StreamReader("/json/ostacoloMushroom.json"))
            {
                string json = r.ReadToEnd();
                mushroom = JsonConvert.DeserializeObject<List<Item>>(json);
            };
        }

        //This Method handles the Load and read of the Flower Obstacle JSON File
        private void LoadJSONobstacleFlower()
        {
            using (StreamReader r = new StreamReader("/json/ostacoloFlower.json"))
            {
                string json = r.ReadToEnd();
                flower = JsonConvert.DeserializeObject<List<Item>>(json);
            };
        }

        //This Method handles the Load and read of the Cactus Obstacle JSON File
        private void LoadJSONostacoloCactus()
        {
            using (StreamReader r = new StreamReader("/json/ostacoloCactus.json"))
            {
                string json = r.ReadToEnd();
                cactus = JsonConvert.DeserializeObject<List<Item>>(json);
            };
        }


        //This Method handles the Load and read of the Power Up Mushroom Obstacle JSON File
        private void LoadJSONobstaclePowerUpMushroom()
        {
            using (StreamReader r = new StreamReader("/json/powerMushroom.json"))
            {
                string json = r.ReadToEnd();
                mushroomPower = JsonConvert.DeserializeObject<List<Item>>(json);
            };
        }

        //This Class is required for a correct JSON Load and DeserializeObject
        class Item
        {
            public string imagePath;
            public double width;
            public double height;
        }
    }
}