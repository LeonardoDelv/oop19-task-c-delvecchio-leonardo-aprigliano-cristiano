using System.Drawing;

namespace Cristiano_Aprigliano.superMarioRun.ApriglianoApp
{
    interface IObstacleController
    {
        /*This Method is required to generate a random Obstacle between the 4 obstacles available based on Math.Random
        This corresponds to "List<ObstacleView> StartObstacleView()" in the Java Project.*/
        List<ObstacleImpl> GenerateRandomObstacle();

        //This Method returns a List<ObstacleImpl> with all the obstacles generated
        List<ObstacleImpl> GetObstacleList();

        //This Method Generates a Mushroom Obstacle
        ObstacleImpl GenerateMushroom();

        //This Method Generates a Flower Obstacle
        ObstacleImpl GenerateFlower();

        //This Method Generates a Cactus Obstacle
        ObstacleImpl GenerateCactus();

        //This Method Generates a PowerUp Mushroom
        ObstacleImpl GeneratePowerUpMushroom();
    }
}